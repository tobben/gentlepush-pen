
$fn=20;

//pen();
module pen(){
  cylinder(d1=0.5, d2=2.35, h=2.4);
  translate([0,0,2.4])
    cylinder(d=2.35, h=5.8);
  translate([0,0,2.4 + 5.8])
    cylinder(d=3, h=97);
  translate([-2, -1, 32])
    cube([4, 2, 4]);
}

//translate([5,0,0])
//pen_carving();
module pen_carving(){
  cylinder(d=2.75, h=5.8 + 2.4 + 0.1);
  translate([0,0,2.4 + 5.8])
    cylinder(d=3.4, h=32 - 2.4 - 5.8);
  translate([0,0,32])
    cylinder(d=4.9, h=21+5);
  translate([0,0,2.4 + 5.8])
    cylinder(d=3.4, h=97);
}

cubeheight = 32+25+5+3-2.4-5;
chamfer_angle = 4;

module chamfer_cube(lift, rotz){
  translate([-7/2, 7/2, lift])
    difference(){
      cube([5, 1, 6]);
      translate([7/2,0,-1])
        rotate([0,0,rotz])
          translate([0,-13,0])
            cube(13);
    }
}

module halfbox(){
  difference(){
    translate([-7/2,-7/2,0])
      cube([7, 7, cubeheight]);
    translate([0,0,-2.4-2])
      pen_carving();
    translate([0, -5, -1])
      cube([10, 10, 100]);
  }
}

rotate([0,-90,0])
box_half_0();
module box_half_0(){
  halfbox();
  for(k = [0,1]) mirror([0,k,0])
    for(z = [6.1, cubeheight - 6 - 6.1])
      chamfer_cube(z, chamfer_angle);
}

translate([0,12,0])
rotate([0,-90,0])
box_half_1();
module box_half_1(){
  halfbox();
  for(k = [0,1]) mirror([0,k,0])
    for(z = [0, cubeheight - 6])
      chamfer_cube(z, chamfer_angle);
}
